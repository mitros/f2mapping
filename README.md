# README #



### What is this repository for? ###


* scripts used in the creation of a Xenopus tropicalis map
* genetic maps used in the v9 genetic mapping paper https://doi.org/10.1016/j.ydbio.2019.03.015
and the version 10 genome assembly paper.


### How do I use these scripts? ###

Usage:
./pairedBarcode.pl <-f forward.reads> <-r reverse.reads> <-b barcode1File>  <-o overhang> <-p outputPrefix> <<-n npos>> <<-s flip ascii offset to sanger(33) from illumina(64)>>
./radDecon.pl <-s sequence.txt> <-b barcodeFile> <-o overhang> <<-n npos>> <<-m minLength>> <<-t sequences to trunc (csl like CTGCAG,TCGTATGCCGTCTTCT)>> <<-f flip ascii offset to sanger(33) from illumina(64)>>

(note:  reads deposited as PRJNA526297 have already been stripped of their barcodes.

### Who do I talk to? ###

* contact tmitros@berkeley.edu
