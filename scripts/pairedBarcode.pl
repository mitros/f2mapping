#!/usr/bin/perl -w
#use strict;
my $init = localtime(time);
print STDERR "started $init\n";
use Getopt::Std;
my %opts = ();
my $validline = getopts('f:r:b:p:o:n:s',\%opts);
my $usage = "$0 <-f forward.reads> <-r reverse.reads> <-b barcode1File>  <-o overhang> <-p outputPrefix> <<-n npos>> <<-s flip ascii offset to sanger(33) from illumina(64)>>\n";
my $seq_data1 = $opts{'f'};
my $seq_data2 = $opts{'r'};
my $bar_data = $opts{'b'};
my $overhang = $opts{'o'};
$overhang = uc($overhang);
my $pre = $opts{'p'};
my $flip = 1 if $opts{'s'};
my $overlen = length($overhang);
my $npos =  $opts{'n'} if $opts{'n'};
die $usage unless ($seq_data1 && $seq_data2 && $bar_data &&  $overhang && $pre);
if ($seq_data1 =~ /\.gz$/) {
  open(FH1, "gunzip -c $seq_data1 |") || die "can't open pipe to $seq_data1";
} else {
  open(FH1, $seq_data1) || die "can't open pipe to $seq_data1\n";
}
if ($seq_data2 =~ /\.gz$/) {
  open(FH2, "gunzip -c $seq_data2 |") || die "can't open pipe to $seq_data2";
} else {
  open(FH2, $seq_data2) || die "can't open pipe to $seq_data2\n";
}
open(STATS, ">$pre.stats");
open(LOG, ">$pre.log");
my (%barcode, %exp, %types,%countWell);
%types = (
  'DIF' => 1,
  'NO' => 1,
);
my $maxlen = inputBarcodes($bar_data);
my @fh = (keys(%types));
foreach my $f (@fh) {
 foreach my $n (1,2) {
  my $fn = "$f.$n";
  open($fn,">$pre.$f.$n.fastq");
  unless ($f eq "NO" || $f eq "DIF"){
    my $fn = "$f.np.$n" unless ($f eq "NO" || $f eq "DIF");
    open($fn,">$pre.$f.np.$n.fastq");
  }
 }
}

my %degen = (
W=>'[AT]',
S=>'[CG]',
M=>'[AC]',		
K=>'[GT]',
R=>'[AG]',	
Y=>'[CT]',
B=>'[CGT]',
D=>'[AGT]',
H=>'[ACT]',
V=>'[ACG]',	
N=>'[ACGT]',
A=>'[A]',
T=>'[T]',
G=>'[G]',
C=>'[C]',
);
while ( defined(my $head1=<FH1>) && defined(my $head2=<FH2>) ) {
 my $seq1 = <FH1>;
 my $space1 = <FH1>;
 my $qual1 = <FH1>;
 my $seq2 = <FH2>;
 my $space2 = <FH2>;
 my $qual2 = <FH2>;
 my ($name1,$name2);
 if ($head1=~/@([^#\s]+)#/){ 
  $name1 = $1; 
 }elsif ($head1=~/@(\S+)/) {
  $name1 = $1;
 }
 if ($head2=~/@([^#\s]+)#/){ 
  $name2 = $1; 
 }elsif ($head2=~/@(\S+)/) {
  $name2 = $1;
 }
 if ($name1 eq $name2) { 
  my %read;
  $read{1} = [$head1, $seq1, $space1, $qual1];
  $read{2} = [$head2, $seq2, $space2, $qual2];
  my $matchOver;
  my @let=split(//,$overhang);
  foreach my $l (@let) {
    $matchOver.=$degen{$l};
  }
  my %bar;
  my %tests;
  foreach my $r (keys (%read)) {
    my $tester = substr($read{$r}->[1], 0, $maxlen);
    $tests{$r}=$tester;
    my $bar=findBar($tester,$matchOver);
    if ($bar) {
      $bar{$r}=$bar;
      my $chop = $barcode{$bar}->[0];
      my $chopped_read = substr($read{$r}->[1], $chop); 
      my $chopped_qual = substr($read{$r}->[3], $chop);
      $chopped_qual = flipQual($chopped_qual) if ($flip);
      $read{$r}[1]=$chopped_read;
      $read{$r}[3]=$chopped_qual;
    }
  }
  if ($bar{1}) {
   if ($read{1}[0]=~/\s1:\w:\w+:/) {
    $read{1}[0]=~s/(\s1:\w:)\w+:/ $1$bar{1}/;
   }elsif ($read{1}[0]=~/#\w+\/1/) {
    $read{1}[0]=~s/#\w+\/1/#$bar{1}\/1/;
   }else {
    print STDERR "can't match header format$read{1}[0]";
   }
   if ($bar{2})  {
    if ($read{2}[0]=~/\s2:\w:\w+:$/) {
     $read{2}[0]=~s/(\s2:\w:)\w+:$/ $1$bar{2}/;
    }elsif ($read{1}[0]=~/#\w+\/2/) {
     $read{2}[0]=~s/#\w+\/2/#$bar{2}\/2/;
    }else {
     print STDERR "can't match header format$read{2}[0]";
    }
    if ($bar{1} eq $bar{2}) {
      my $fileName = $barcode{$bar{1}}->[1];
      &printPairs(\%read,$fileName);
      $countWell{$fileName}++;
    }else {
      print LOG "diffbar $bar{1} $bar{2} $barcode{$bar{1}}->[1] $barcode{$bar{2}}->[1]\n";
      my $fileName = "DIF";
      printPairs(\%read,$fileName);
      $countWell{$fileName}++;
    }
   }else { # bar1 only
     print LOG "bar1 $tests{1} $tests{2}\n";
     my $fileName = $barcode{$bar{1}}->[1].".np";
     printPairs(\%read,$fileName,1);
     $countWell{$fileName}++;
   }
  }elsif ($bar{2}) {
    print LOG "bar2 $tests{1} $tests{2}\n";
    my $fileName = $barcode{$bar{2}}->[1].".np";
    printPairs(\%read,$fileName,2);
    $countWell{$fileName}++;
  }else {
    print LOG "no barcode $tests{1} $tests{2}\n";
    my $fileName = "NO";
    printPairs(\%read,$fileName);
    $countWell{$fileName}++;
  }
 }else {
   print LOG "notpaired $name1 $name2\n";
 }
}
close FH1;
close FH2;
$exp{NO} = "nobarcode"; 
$exp{DIF} = "diffbarcode"; 
my $total=0;
foreach my $w (sort {$countWell{$b} <=> $countWell{$a}} keys(%countWell)) {
  print STATS "$w\t$countWell{$w}\t$exp{$w}\n";
  $total+=$countWell{$w};
}
print STATS "\nTOTAL\t$total\n";
my $end = localtime(time);
print STDERR "finished $end\n";
sub printPairs {
  my ($rref,$name) = @_;
  foreach my $i (1,2) {
    my $fh = "$name.$i";
    my $r = $rref->{$i};
    printFH($fh,@$r);
  }
}
sub printFH {
  my $fh=shift;
  my @fields= @_;
  print $fh @fields;
}

sub inputBarcodes {
  my $file = shift;
  open(F,$file) || die "can't open $file\n";
  my $maxCode=0;
  while (<F>) {
    chomp;
    my ($exp,$bar,@a)=split(/\s+/,$_);
    $bar = uc($bar);
    $exp =~s/\#/num/g;
    $exp{$exp}=$bar;
    my $np = "$exp.np";
    $exp{$np}=$bar;
    $types{$exp}++;
    my $lenBar=length($bar);
    my $lenHang = length($overhang);
    my $seq = $bar;
    my $lenCode = $lenBar + $lenHang;
    $maxCode = $lenCode if ($lenCode > $maxCode);
    $barcode{$seq} = [$lenBar,$exp,$bar];
#print STDERR "barcode{$seq} =     $lenBar,$exp,$bar\n";
    if (defined($npos)) {
        my $p = $npos-1;
	my @barc= split(//,$bar);
	$barc[$p]="N";
	my $newb = join("",@barc);
        $seq = $newb;
        $barcode{$seq} = [$lenBar,$exp,$bar];
#print STDERR "N barcode{$seq} =     $lenBar,$exp,$bar\n";
    }
  }
  close F;
  return $maxCode;
}

sub read_file_line {
 my $fh = shift;
 if ($fh and my $line = <$fh>) {
  return $line;
 }
 return;
}

sub findBar {
  my ($tester,$cutsite)=@_;
  if ($tester=~/([ATGCN]+)$cutsite/i) {
    my $test = $1;
    if ($barcode{$test}) {
      my $bar = $barcode{$test}->[2];
      return ($bar);
    }
  }else {
    print LOG "nocut $tester\n";
  }
  return;
}

sub flipQual {
  my $qual = shift;
  chomp $qual;
  my @num = unpack("C*",$qual);
  my @newasc;
  foreach my $n (@num) {
    my $nn = $n-31;
    push(@newasc,chr($nn));
  }
  my $newqual = join("",@newasc);
  $newqual.="\n";
  return ($newqual);
}
