#!/usr/bin/perl -w
#use strict;
use Getopt::Std;
my %opts = ();
my $validline = getopts('s:b:o:t:m:n:f',\%opts);
my $usage = "$0 <-s sequence.txt> <-b barcodeFile> <-o overhang> <<-n npos>> <<-m minLength>> <<-t sequences to trunc (csl like CTGCAG,TCGTATGCCGTCTTCT)>> <<-f flip ascii offset to sanger(33) from illumina(64)>>\n";
my $seq_data = $opts{'s'};
my $bar_data = $opts{'b'};
my $overhang = $opts{'o'};
$overhang = uc($overhang);
my $flip = 1 if $opts{'f'};
my $overlen = length($overhang);
my $npos =  $opts{'n'} if $opts{'n'};
my $cuts ="";
$cuts=  $opts{'t'} if $opts{'t'};
my @cuts=split(",",$cuts);
my $trunc=join("|",@cuts) if @cuts;
#print STDERR "trunc top $trunc\n";
my $minLen=35;
$minLen= $opts{'m'} if $opts{'m'};
die $usage unless ($seq_data && $bar_data && $overhang);

open(SEQDATA, $seq_data);
open(OTHER, ">$seq_data.otherreads.fastq");
open(STATS, ">$seq_data.stats");
open(LOG, ">$seq_data.log");
my (%barcode, %exp, %types,%countWell);
$exp{'other'}="";
my $maxlen = inputBarcodes($bar_data);
my @keys=keys(%barcode);
my @fh = (keys(%types));
foreach my $f (@fh) {
  open($f,">$f.fastq");
}

my %degen = (
W=>'[AT]',
S=>'[CG]',
M=>'[AC]',		
K=>'[GT]',
R=>'[AG]',	
Y=>'[CT]',
B=>'[CGT]',
D=>'[AGT]',
H=>'[ACT]',
V=>'[ACG]',	
N=>'[ACGT]',
A=>'[A]',
T=>'[T]',
G=>'[G]',
C=>'[C]',
);
while (<SEQDATA>) {
  my $header = $_;
  my $sequence = <SEQDATA>; 
  my $spacer = <SEQDATA>; 
  my $quality = <SEQDATA>;
  my @read = ($header, $sequence, $spacer, $quality);
  my $testlen = $maxlen;
  my $maxId = substr($read[1], 0, $testlen);
  if ($maxId=~/N/) {
    print LOG "N in id $maxId $header";
  }
  my $bar='other';
  my $matchOver;
  my @let=split(//,$overhang);
  foreach my $l (@let) {
    $matchOver.=$degen{$l};
  }
  #my @matches = $sequence=~/$matchOver/g;
  #my @matches= $sequence=~/$trunc/g;
  #  push(@matches,@seen);
  #my $matches=scalar(@matches);
  #if ($matches >0) {
  #  print LOG "$matches\tmatches\t@matches\t$header$sequence";
  #}else {
   while ($bar eq 'other' && $testlen>4) {
    my $tester = substr($read[1], 0, $testlen);
    if ($tester=~/$matchOver$/) {
      my $id = substr($tester,0,-$overlen);
      if ($barcode{$id}) {
        $bar=$id;
      }else {
        print LOG "cutEnd $id not found in $tester $header";
	$testlen--;
      }
    }else {
      $testlen--;
    }
   }
  #}
  my $chop;
  if ($bar eq 'other') {
     print LOG "no barcode $maxId $header";
  }else {
     $chop = $barcode{$bar}->[0];
  }
  if ($chop) {	
    my $bc = substr($read[1], 0, $chop);
    chomp $read[0];
    my $new_header = $read[0]."\n"; 
    my $chopped_read = substr($read[1], $chop); #starts reading from the position after the barcode...this is the sequence we want
    my $chopped_quality = substr($read[3], $chop); 
    my $newqual;
    if ($flip) {
      chomp $chopped_quality;
      my @num = unpack("C*",$chopped_quality);
      my @newasc;
      foreach my $n (@num) {
        my $nn = $n-31;
	push(@newasc,chr($nn));
      }
      $newqual = join("",@newasc);
      $newqual.="\n";
    }else {
      $newqual = $chopped_quality;
    }
    if (defined($trunc)) {
      if ($chopped_read=~/(^\S?)$trunc/){
        $chopped_read = $1;
        my $slength=length($chopped_read);
        $newqual=substr($newqual,0,$slength);
        print LOG "truncated $new_header $sequence";
      }
    }
    $countWell{$barcode{$bar}->[2]}++;
    my $len=length($chopped_read);
    if ($len>=$minLen) {
      printFH($barcode{$bar}->[1], $new_header, $chopped_read, $read[2], $newqual);
    }else {
      print LOG "tooshort $new_header,$sequence";
    }
  }else {
    $countWell{'other'}++;
    chomp $read[0];
    my $new_header = $read[0] . " $maxId\n"; 
    print OTHER "$new_header", "$read[1]", "$read[2]", "$read[3]";
  }
}
my $total=0;
foreach my $w (sort {$countWell{$b} <=> $countWell{$a}} keys(%countWell)) {
  my $wh=$w;
  print STATS "$w\t$countWell{$w}\t$exp{$w}\n";
  $total+=$countWell{$w};
}
print STATS "\nTOTAL\t$total\n";

sub printFH {
  my $fh=shift;
  my @fields= @_;
  print $fh @fields;
}

sub inputBarcodes {
  my $file = shift;
  open(F,$file) || die "can't open $file\n";
  my $maxCode=0;
  while (<F>) {
    chomp;
    my ($lib,$lane,$bar,$exp,@a)=split(/\s+/,$_);
    $bar = uc($bar);
    $exp{$bar}=$exp;
    $types{$exp}++;
    my $lenBar=length($bar);
    my $lenHang = length($overhang);
    my $seq = $bar;
    my $lenCode = $lenBar + $lenHang;
    $maxCode = $lenCode if ($lenCode > $maxCode);
    $barcode{$seq} = [$lenBar,$exp,$bar];
#print STDERR "barcode{$seq} =     $lenBar,$exp,$bar\n";
    if (defined($npos)) {
        my $p = $npos-1;
	my @barc= split(//,$bar);
	$barc[$p]="N";
	my $newb = join("",@barc);
        $seq = $newb;
        $barcode{$seq} = [$lenBar,$exp,$bar];
#print STDERR "N barcode{$seq} =     $lenBar,$exp,$bar\n";
    }
  }
  close F;
  return $maxCode;
}
